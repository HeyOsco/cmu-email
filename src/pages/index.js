import { useState } from 'react';
// NextJS Imports
import Head from 'next/head';
import Link from 'next/link'
// Config Imports
import { SITE_ABOUT, SITE_TITLE } from '../lib/config';
// Component Imports
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Layout, { siteTitle } from '../components/layout';
// Styling Imports
import utilStyles from '../styles/utils.module.css';

export default function Home() {
  const [email, setEmail] = useState(null); 
  const [loading, setLoading] = useState(false); 
  const [status, setStatus] = useState(null); 

  const mystyle = {
    // color: "white",
    // backgroundColor: "DodgerBlue",
    padding: "5px 50px 25px 50px",
    // fontFamily: "Arial"
  };

  const joinNewsletter = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (!email){ 
      alert("Please fill in an email!")
    }
    else {
      // Add User to Database if they do not exist
      console.log("--Adding user to DB...")
      console.log("email: " + email)
      await fetch("../api/post-email", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"email": email}),
      });
      setStatus("complete")
      console.log("--Adding user complete!")
    }
    setLoading(false)
  };

  return (
    <div>
      <Layout home>
        <Head>
          <title>{siteTitle}</title>
        </Head>        
        <Paper elevation={3} style={mystyle}>
          <section className={utilStyles.headingMd}>
            <center>
            <h3><i>{SITE_TITLE}</i></h3>
            <h4>{SITE_ABOUT}</h4>
              <br/>
              {(status === "complete") && 
                  <div>
                    <Alert severity="success">Successfully joined newsletter! We'll see you soon at orientation, new student :3 </Alert> 
                    <br/>
                  </div>
                }
              <TextField
                  disabled={loading}
                  id="filled-disabled"
                  label="Email"
                  // defaultValue={VR_SAMPLE_PROMPT}
                  variant="outlined"
                  // fullWidth
                  onChange={(e) => setEmail(e.target.value)}
                />
              <br/>
              <br/>
              <Button variant="contained" color="primary" onClick={(e) => joinNewsletter(e)}>Join Newsletter</Button>
              {/* 
              <br/>
              <br/>
              <Link href="https://www.clown-math.university/episodes/season-0/episode-1">{"Episode 0"}</Link>
              <br/>
              <br/>
              <Link href="https://myujen.com/explore">{"Explore Campus"}</Link>
              <br/>
              <br/>
              <Link href="https://www.clown-math.university/socials/">{"Socials"}</Link> 
              */}
          </center>
          </section>
        </Paper>
      </Layout>
    </div>
  );
}
