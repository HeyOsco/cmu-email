const SITE_ABOUT = "Join the CMU Newsletter to get our episode releases and monthly updates!"
const SITE_NAME = "Clown Math Univeristy"
const SITE_TITLE = "Newsletter Sign Up"

export {
  SITE_ABOUT,
  SITE_NAME,
  SITE_TITLE,
}